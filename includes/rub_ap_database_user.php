<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 25.08.2015
 * Time: 17:31
 */

global $wpdb;
global $rub_ap_table_name_user;
$rub_ap_table_name_user = $wpdb->prefix . "RUB_ap_user_table";
global $rub_ap_table_name_wpposts;
$rub_ap_table_name_wpposts = $wpdb->prefix . "posts";


/**
 * Creates a table which stores the information who originally wrote the anonymous post
 */
function rub_ap_create_user_table(){

    global $wpdb;
    global $rub_ap_table_name_user;

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $rub_ap_table_name_user (
      id bigint(20) NOT NULL AUTO_INCREMENT,
      wp_user_id bigint(20),
      wp_post_id bigint(20),
      timestamp_added datetime DEFAULT '0000-00-00 00:00:00',
      UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
}

/**
 * Use to add new entries into the table
 * @param $wp_user_id
 * @param $wp_post_id
 */
function rub_ap_add_new_entry($wp_user_id, $wp_post_id){

    global $wpdb;
    global $rub_ap_table_name_user;

	// check whether post already exists in this table, if yes, skip adding again
	$results = $wpdb->get_var($wpdb->prepare(
		"SELECT COUNT(*)
		FROM $rub_ap_table_name_user
		WHERE wp_post_id = %s",
		$wp_post_id
	));

	echo $results;

	if($results > 1) {
		// return;
	}

	$wpdb->insert(
        $rub_ap_table_name_user,
        array(
            'wp_user_id' => $wp_user_id,
            'wp_post_id' => $wp_post_id,
            'timestamp_added' => current_time('mysql'),
        )
    );
}

/**
 * Fetch the original author of an anonymous post
 * @param $wp_post_id
 * @return null|string
 */
function rub_ap_get_original_author_by_post($wp_post_id){

    global $wpdb;
    global $rub_ap_table_name_user;

	// Limit 1 since each save adds an entry.
    $original_author = $wpdb->get_var( $wpdb->prepare(
        "SELECT wp_user_id
		FROM $rub_ap_table_name_user
		WHERE wp_post_id = %s
		LIMIT 1
	    ",
        $wp_post_id
    ));

    return $original_author;
}

/**
 * Fetch all anonymized posts by given user
 * @param $wp_user_id
 * @return mixed
 */
function rub_ap_get_posts_by_author($wp_user_id){

    global $wpdb;
    global $rub_ap_table_name_user;

    $posts = $wpdb->get_results($wpdb->prepare(
        "SELECT wp_post_id
        FROM $rub_ap_table_name_user
        WHERE wp_user_id = %s
        ",
        $wp_user_id
    ));

    return $posts;
}

/**
 * Use this to remove an entry of original user for post from this table
 * @param $wp_post_id
 */
function rub_ap_remove_entry_by_post($wp_post_id){

	global $wpdb;
	global $rub_ap_table_name_user;

	$wpdb->delete(
		$rub_ap_table_name_user,
		array(
			'wp_post_id' => $wp_post_id
		)
	);
}

/**
 * Custom method to insert new author id into wp-posts table. This prevents the post_modified date from updating,
 * used when updating revisions, since we only want to hide the author not "modify" the content
 * @param $new_author_id
 * @param $wp_post_id
 */
function rub_ap_update_wppost($wp_post_id, $new_author_id){

    global $wpdb;
    global $rub_ap_table_name_wpposts;

    $wpdb->update(
        $rub_ap_table_name_wpposts,
        array(
            'post_author' => $new_author_id,
        ),
        array(
            'ID' => $wp_post_id
        )
    );
}