<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 21.08.2015
 * Time: 15:35
 */

/**
 * Display checkbox on website
 */
function rub_ap_checkbox_in_reply(){
	$text = __(' Post Anonymously','RUB_Anonymous_Posting');
    $original_author = rub_ap_get_original_author_by_post(bbp_get_reply_id());

    // if this is a edit of an anon-post, auto tick the anon-box to prevent users from accidently posting with clear name
    // capability is checked in rub_ap_filter_user_has_cap(), thus no need to check here whether author is valid
	if(bbp_is_edit()) {

        // is post already set to anon
        if(bbp_get_reply(bbp_get_reply_id())->post_author == get_option('RUB_ap_anon_user_id')) {

            // Dont let admins modify anonymity
            if ($original_author == get_current_user_id()) {
                echo "<label><input type='checkbox' id='rub_ap_post_anonymously' name='rub_ap_post_anonymously' value='1' checked='checked' onchange='rub_ap_handle_checkbox_change(this);'>$text</label>";
                echo "<div id='rub_ap_non_anon_warning' class='badge' style='display:none;background-color:tomato'>" . __('WARNING: You are about to remove the anonymity of this post!', 'RUB_Anonymous_Posting') . "</div>";
            } else {
                // e.g. if an admin is editing this, dont let him edit the anonymity!
                echo "<label><input type='checkbox' id='rub_ap_post_anonymously' name='rub_ap_post_anonymously' value='1' checked='checked' onclick='return false;'>$text</label>";
                echo " ";
                _e('(Only the original author of a post can edit the anonymity!)', 'RUB_Anonymous_Posting');
            }
        } else { // if its not anon, dont let admin change it anyway
            // let original users do their stuff, but hinder admins
            if (bbp_get_reply(bbp_get_reply_id())->post_author == get_current_user_id()) {
                echo "<label><input type='checkbox' id='rub_ap_post_anonymously' name='rub_ap_post_anonymously' value='1'>$text</label>";
            } else {
                // e.g. if an admin is editing this, dont let him edit the anonymity!
                echo "<label><input type='checkbox' id='rub_ap_post_anonymously' name='rub_ap_post_anonymously' value='1' onclick='return false;'>$text</label>";
                echo " ";
                _e('(Only the original author of a post can edit the anonymity!)', 'RUB_Anonymous_Posting');
            }
        }
	} else {
	    // not an edit, then display checkbox normally
        echo "<label><input type='checkbox' id='rub_ap_post_anonymously' name='rub_ap_post_anonymously' value='1'>$text</label>";
	}
}
add_action('bbp_theme_before_reply_form_submit_wrapper', 'rub_ap_checkbox_in_reply');

/**
 * Catch post and update author
 * @param $reply_id
 * @return null|WP_Post
 */
function rub_ap_set_anonymous_user_in_new_reply($reply_id){

	if ($_POST['rub_ap_post_anonymously'] == '1'){

        $new_user_id = get_option('RUB_ap_anon_user_id');

		// anon already set? no need to update again
		if(bbp_get_reply($reply_id)->post_author == $new_user_id){
			return;
		}

		$updated_post = array(
			'ID' => $reply_id,
			'post_author' => $new_user_id,
		);

		// save old value to db before updating
		rub_ap_add_new_entry(get_current_user_id(), $reply_id);

		wp_update_post($updated_post);

        do_action('rub_ap_reply_anonymity_set_in_initial_post', $reply_id);
    }
}
add_action('bbp_new_reply', 'rub_ap_set_anonymous_user_in_new_reply', 10, 1);

/**
 * Updates anon-status after saving an edited reply, either adding anon status or removing it.
 * @param $reply_id
 */
function rub_ap_set_anonymous_user_in_edit_reply($reply_id){

	if($_POST['rub_ap_post_anonymously'] == '1'){

        $anon_user = get_option('RUB_ap_anon_user_id');
        $post_author = bbp_get_reply($reply_id)->post_author;

        // anon already set? no need to update again
        if($post_author == $anon_user){
            do_action('rub_ap_reply_anonymity_kept_in_edit', $reply_id);
            return;
        } else {

            $updated_post = array(
                'ID' => $reply_id,
                'post_author' => $anon_user,
            );

            // save old value to db before updating
            rub_ap_add_new_entry(get_current_user_id(), $reply_id);

            wp_update_post($updated_post);

            rub_ap_set_revision_anonymity($reply_id);

            do_action('rub_ap_reply_anonymity_set_in_edit', $reply_id);
        }
	}

	if($_POST['rub_ap_post_anonymously'] == ''){

		// original author
		$original_author = rub_ap_get_original_author_by_post($reply_id);

		// if original author is set, the entry was anon once, if not, it never was anon, continue
		if(is_null($original_author)){
			return;
		} else {

			// set old author again
			$updated_post = array(
				'ID' => $reply_id,
				'post_author' => $original_author,
			);

            // update revisions
            rub_ap_unset_revision_anonymity($reply_id);

			wp_update_post($updated_post);

			// remove from our db table
			rub_ap_remove_entry_by_post($reply_id);

            do_action('rub_ap_reply_anonymity_unset_in_edit', $reply_id);
        }
	}
}
add_action('bbp_edit_reply', 'rub_ap_set_anonymous_user_in_edit_reply', 10, 1);