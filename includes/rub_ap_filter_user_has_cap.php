<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 26.08.2015
 * Time: 11:20
 */

/**
 * This method adds the possibility to users to edit their anon-replies through giving them the capability of editing others posts for that specific post
 * @param $allcaps
 * @param $caps
 * @param $args
 *
 * @return mixed
 */
function rub_ap_filter_user_has_cap($allcaps, $caps, $args){
/*
 * @param array $args
 * [0] Requested capability
 * [1] User ID
 * [2] Associated object ID
 */

	// jump out if object id not set
	if(!array_key_exists(2,$args)) {
		return $allcaps;
	}

	// Get post_id from args, be outside loop to save edit properly
	$id = $args[2]; // post ID

	if(bbp_is_reply($id) || bbp_is_topic($id)){
		// account for edits of topics and replies
		if(bbp_is_reply($id)) {
			$author = bbp_get_reply($id)->post_author;
		}

		if(bbp_is_topic($id)){
			$author = bbp_get_topic($id)->post_author;
		}

		// Proceed if post is authored by anonymous
		if($author == get_option('RUB_ap_anon_user_id')){

			// check if current user is the original author
			$original_user = rub_ap_get_original_author_by_post($id);

			if(get_current_user_id() == $original_user) {

				// add capability to edit replies
				if(bbp_is_reply($id)) {
					$allcaps['edit_others_replies'] = 1;
				}

				if(bbp_is_topic($id)){
					$allcaps['edit_others_topics'] = 1;
				}
			}
		}
	}

	return $allcaps;
}
add_filter('user_has_cap', 'rub_ap_filter_user_has_cap', 10, 3);