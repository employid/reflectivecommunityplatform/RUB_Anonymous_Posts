<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 25.08.2015
 * Time: 17:49
 */

/**
 * Filter the link of the author, if user is anonymous so that users can access that profile easily
 * http://www.matrudev.com/post/remove-bbpress-profile-url-link/
 * @param $author_link
 * @param $args
 * @return mixed
 */
function rub_ap_filter_author_link($author_link, $args){

	// use post_id if specified, otherwise assume that we are in loop and thus take current reply id...safe?!

	// jump out if object id not set or if it is 0
	if(array_key_exists('post_id',$args)) {
        if ($args['post_id'] != 0) {
            $id = $args['post_id'];
        } else {

            if (bbp_is_topic(bbp_get_topic_id())) {
                $id = bbp_get_topic_id();
            }

            // check this after topic, since all replies are also topics somehow, so ID gets overwritten here
            if (bbp_is_reply(bbp_get_reply_id())) {
                $id = bbp_get_reply_id();
            }
        }
    } else {
        // so if we dont have a given id, try to obtain it through the loop :)
        $id = get_post()->ID;
    }

	if(get_post($id)->post_author == get_option('RUB_ap_anon_user_id')) {
		$author_link = preg_replace( array( '{<a[^>]*>}', '{}' ), array( " " ), $author_link );
	}

	return $author_link;
}
add_filter('bbp_get_reply_author_link', 'rub_ap_filter_author_link', 10, 2);
add_filter('bbp_get_topic_author_link', 'rub_ap_filter_author_link', 10, 2);
add_filter('bbp_get_author_link', 'rub_ap_filter_author_link', 10, 2);