/**
 * Created by blunk on 27.08.2015.
 */

function rub_ap_handle_checkbox_change(checkbox) {
    if(checkbox.checked == false){
        document.getElementById('rub_ap_non_anon_warning').style.display = 'inline';
    } else {
        document.getElementById('rub_ap_non_anon_warning').style.display = 'none';
    }
}