<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 27.08.2015
 * Time: 09:32
 */

/**
 * Modifies saving of revision to cloak original author, but accounts for admin edits or admin anon posts
 * @param $post_id
 *
 * @return null|WP_Post
 */
function rub_ap_filter_revision($post_id) {

    // This method is also being called when a normal post is being saved, since there are also revisions involved!

	// check if post is a revision
	if(wp_is_post_revision($post_id)){

        // Revision here has still the original author even if its saved as anon
		$revision_post = get_post($post_id);

		// unhook this function so it doesn't loop infinitely (because wp_update_post triggers save_post)
		remove_action( 'save_post', 'rub_ap_filter_revision', 10, 1 );

		// ask for it once, don't know how good caching is
		$anon_user = get_option('RUB_ap_anon_user_id');

		// check whether revision should be anon, if yes update posts table
		if($_POST['rub_ap_post_anonymously'] == '1'){

            // Check whether original author is current user, to exclude admin edits
            $post_parent_id = $revision_post->post_parent;
            $post_parent_author = get_post($post_parent_id)->post_author;

            // if it already is anon, check who is the original author
            if($post_parent_author == $anon_user){
                $post_parent_author = rub_ap_get_original_author_by_post($post_parent_id);
            }

            // if original one is current one, update this revision
            if($post_parent_author == get_current_user_id()){
                rub_ap_update_wppost($post_id, $anon_user);
                rub_ap_add_new_entry(get_current_user_id(), $post_id);
                do_action('rub_ap_revision_anonymity_set', $post_id);
            }
		}

		// in case anon gets removed
		if($_POST['rub_ap_post_anonymously'] == ''){

            // update the one currently caught
            rub_ap_update_wppost($revision_post->ID, get_current_user_id());
            do_action('rub_ap_revision_anonymity_unset', $post_id);
        }

		// re-hook this function
		add_action( 'save_post', 'rub_ap_filter_revision', 10, 1 );
	}

	return get_post($post_id);
}
add_action( 'save_post', 'rub_ap_filter_revision', 10 , 1);

function rub_ap_set_revision_anonymity($post_id){
    $anon_user = get_option('RUB_ap_anon_user_id');

/*
    // Check whether original author is current user, to exclude admin edits
    $post_parent_id = $revision_post->post_parent;
    $post_parent_author = get_post($post_parent_id)->post_author;

    // if it already is anon, check who is the original author
    if($post_parent_author == $anon_user){
        $post_parent_author = rub_ap_get_original_author_by_post($post_parent_id);
    }
*/
    $other_revisions = get_posts(array(
        'post_type' => 'revision',
        'post_parent' => $post_id,
        'post_author' => get_current_user_id(),  // keep admin edits out of the way, unless the admin wrote it <<< this filter bit doesnt seem to work
        'post_status' => 'inherit',              // overwrite default of get_posts
        'posts_per_page' => 9999,                // lets hope no author writes more than 10k revisions :)
    ));

    foreach($other_revisions as $revision){

        // better check again
        if($revision->post_author == get_current_user_id()){
            $revision_id = $revision->ID;

            rub_ap_update_wppost($revision_id, $anon_user);
            rub_ap_add_new_entry(get_current_user_id(),$revision->ID);

            do_action('rub_ap_revision_anonymity_set', $revision_id);
        }
    }
}

function rub_ap_unset_revision_anonymity($post_id){
    // $current_post = get_post($post_id);
    $anon_user = get_option('RUB_ap_anon_user_id');

    $other_revisions = get_posts( array(
        'post_type'      => 'revision',
        'post_parent'    => $post_id,
        'post_author'    => $anon_user,
        'post_status'    => 'inherit',  // overwrite default of get_posts
        'posts_per_page' => 9999,       // lets hope no author writes more than 10k revisions :)
    ));

    foreach ( $other_revisions as $revision ) {

        // check if entry was anonymized
        if($revision->post_author == $anon_user){
            $revision_id = $revision->ID;

            // update revision with original author
            rub_ap_update_wppost($revision_id, rub_ap_get_original_author_by_post($revision_id));

            // and save it in our data table
            rub_ap_remove_entry_by_post($revision_id);

            do_action('rub_ap_revision_anonymity_unset', $revision_id);
        }
    }
}