<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 21.08.2015
 * Time: 16:29
 */

/**
 * Create a new entry in the menu bar for all RUB plugins (if not already created). Then add entry for this plugin.
 */
function rub_ap_admin_menu(){

	// check whether global RUB admin menu page already exists, if not, create it!
	if ( empty ( $GLOBALS['admin_page_hooks']['RUB-reflection-plugins'] ) )
		add_menu_page(
			'RUB Reflection Plugins',
			'RUB Reflection Plugins',
			'manage_options',
			'RUB-reflection-plugins',
			'rub_qs_admin_menu_page_callback'
		);

	// afterwards, add sub entry for this plugin
	add_submenu_page(
		'RUB-reflection-plugins',
		'Anonymous Posts',
		'Anonymous Posts',
		'manage_options',
		'Anonymous-Posts',
		'rub_ap_admin_settings_form'
	);
}
add_action('admin_menu', 'rub_ap_admin_menu');

/**
 * Create settings section to hold the value for questions 1, 2 and 3
 */
function rub_ap_settings(){
	add_settings_section(
		'RUB-ap-settings',
		__('Anonymous Posts Settings','RUB_Anonymous_Posting'),
		'rub_ap_settings_callback',
		'Anonymous-Posts'
	);

	add_settings_field(
		'RUB_ap_anon_user_id',
		__('UserID','RUB_Anonymous_Posting'),
		'rub_ap_text_callback',
		'Anonymous-Posts',
		'RUB-ap-settings',
		array(
			'RUB_ap_anon_user_id' // Should match Option ID
		)
	);

	register_setting('Anonymous-Posts', 'RUB_ap_anon_user_id', 'esc_attr');
}
add_action('admin_init', 'rub_ap_settings');

/**
 * This method handles the display of the settings section in the admin menu
 */
function rub_ap_admin_settings_form() {
	?>
	<div class="wrap">
		<form method="post" action="options.php">
			<?php
			//wp_nonce_field('update-options');
			settings_errors();
			settings_fields( 'Anonymous-Posts' );
			do_settings_sections( 'Anonymous-Posts' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

/**
 * Callback method to display input form for the questions
 * @param $args
 */
function rub_ap_text_callback($args) {
	// dont use translation here, let qTranslate-X do the work!

	$value = get_option($args[0]);
	echo '<input
        type="text"
        id="'.$args[0].'"
        class="'.$args[0].'"
        name="'.$args[0].'"
        size="50"
        value="'.$value.'"
        >';
	echo '<p class="description RUB_ap_settings">'.$args[0].'</p>';
}

/**
 * This callback just shows the description for the settings section
 */
function rub_ap_settings_callback(){
	_e('Enter the user id of the user who should act as the anonymous user for all.', 'RUB_Anonymous_Posting');
}

function rub_ap_anon_user_updated($option, $old_value, $value){

	if($option == 'RUB_ap_anon_user_id'){
		do_action('rub_ap_option_updated_anon_user', $option, $old_value, $value);
	}
}
add_action('updated_option', 'rub_ap_anon_user_updated', 10, 3);