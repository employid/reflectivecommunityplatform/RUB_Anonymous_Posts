<?php

/*
Plugin Name: RUB Anonymous Posts
Plugin URI: http://not-available.yet
Description: This plugin allows bbPress users to post anonymously but retains comfort features like email notifications.
Version: 0.1
Author: Oliver Blunk
Author URI: http://www.imtm-iaw.ruhr-uni-bochum.de/wissenschaftliches-personal/oliver-blunk/
License: All rights reserved
*/

// Show own anon posts in profile, but only when user is viewing the profile..otherwise others can see the post!
// Do the same shit all over again for regular wp-comments if required

include('includes/rub_ap_admin.php');
include('includes/rub_ap_bbpress_reply.php');
include('includes/rub_ap_bbpress_topic.php');
include('includes/rub_ap_database_user.php');
include('includes/rub_ap_filter_profile_links.php');
include('includes/rub_ap_filter_revision.php');
include('includes/rub_ap_filter_user_has_cap.php');

/*
 * create settings upon first activation, but retain values to avoid accidentally messing with experiments
 */
function rub_ap_activate_plugin(){

	// Set it to admin user first (always exists) and then let admin configure it in menu later on
	if(get_option('RUB_ap_anon_user_id') === false){
		update_option('RUB_ap_anon_user_id','1');
	}
}
register_activation_hook(__FILE__, 'rub_ap_activate_plugin');

/*
 * load language files
 */
function rub_ap_load_textdomain() {
	load_plugin_textdomain( 'RUB_Anonymous_Posts', false, basename(dirname(__FILE__)) . '/languages' );
}
add_action('plugins_loaded', 'rub_ap_load_textdomain' );

/**
 * loads all necessary database stuff upon plugin activation
 */
function rub_ap_load_db(){
	rub_ap_create_user_table();
}
register_activation_hook(__FILE__, 'rub_ap_load_db');

/*
 * load js files
 */
function rub_ap_load_js(){
	wp_register_script('rub_ap_no_anon_notification', plugin_dir_url(__FILE__) . 'js/rub_ap_no_anon_notification.js', array('bootstrap','jquery'));
	wp_enqueue_script('rub_ap_no_anon_notification');
}
add_action( 'wp_enqueue_scripts', 'rub_ap_load_js' );

/*
 * Hooks explained:
 * Correct user id wont be given due to anonymity reasons. Use the anonymous user if you must: get_option('RUB_ap_anon_user_id')
 *
 * do_action('rub_ap_revision_anonymity_set', $revision_id)                 => Fired when a revision was updated due to editing topics/replies
 * do_action('rub_ap_topic_anonymity_set_in_initial_post', $topic_id);      => Fired when a topic is set to anonymous, fired once
 * do_action('rub_ap_reply_anonymity_set_in_initial_post', $reply_id);      => Fired when a reply is set to anonymous. fired once
 *
 * do_action('rub_ap_topic_anonymity_kept_in_edit', $topic_id);             => Fired when anonymity isnt removed during edit of a topic, fires each edit
 * do_action('rub_ap_topic_anonymity_set_in_edit', $topic_id);              => Fired when anonymity is set during an edit of a topic, fired once, afterwards the rub_ap_topic_anonymity_kept_in_edit event fires
 * do_action('rub_ap_reply_anonymity_kept_in_edit', $reply_id);             => Fired when anonymity isnt removed during edit of a reply, fires each edit
 * do_action('rub_ap_reply_anonymity_set_in_edit', $reply_id);              => Fired when anonymity is set during an edit of a reply, fired once, afterwards the rub_ap_topic_anonymity_kept_in_edit event fires
 *
 * do_action('rub_ap_topic_anonymity_unset_in_edit', $topic_id)             => Fired when anonymity is removed during editing a topic
 * do_action('rub_ap_reply_anonymity_unset_in_edit', $reply_id)             => Fired when anonymity is removed during editing a reply
 * do_action('rub_ap_revision_anonymity_unset', $revision_id);              => Fired when anonymity is removed from a revision due to removing anonymity of a topic/reply
 *
 * do_action('rub_ap_option_updated_anon_user', $option, $old_value, $value); => Fires when the anon user id is updated in the admin settings
 */